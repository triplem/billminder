package com.droidweb.billminder.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by droidweb on 4/11/14.
 */
public class BillsStoreDatabaseHelper extends SQLiteOpenHelper {
    public static final String TABLE_BILLS = "billsTable";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PAY_TO = "Pay_To";
    public static final String COLUMN_DUE_DATE = "Due_date";
    public static final String COLUMN_ARCHIVED = "Archived_Boolean";
    public static final String COLUMN_RECURRENCE_RULE = "Recurrence_Rule";
    public static final String DATABASE_NAME = "bills.db";
    public static final int DATABASE_VERSION = 6;

    // database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_BILLS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_PAY_TO
            + " text not null, " + COLUMN_DUE_DATE
            + " int not null, " + COLUMN_ARCHIVED
            + " bool not null, " + COLUMN_RECURRENCE_RULE
            + " text);";

    public BillsStoreDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(BillsStoreDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BILLS);
        onCreate(db);
    }
}
