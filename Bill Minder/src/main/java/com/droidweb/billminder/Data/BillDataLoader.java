package com.droidweb.billminder.Data;

import android.content.Context;

import com.droidweb.billminder.Beans.Bill;

import java.util.List;

import javax.sql.DataSource;

/**
 * Created by Droidweb on 8/10/2014.
 */
public class BillDataLoader extends AbstractDataLoader<List<Bill>> {
    private AbstractDataSource<Bill> mDataSource;

    public BillDataLoader(Context context, AbstractDataSource dataSource){
        super(context);
        mDataSource = dataSource;
    }
    @Override
    protected List<Bill> buildList() {
        List<Bill>billList = mDataSource.read();
        return billList;
    }
    public void insert(Bill entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(Bill entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(Bill entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends DataLoadingAsyncTask<Bill, Void, Void> {
        InsertTask(BillDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Bill... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends DataLoadingAsyncTask<Bill, Void, Void> {
        UpdateTask(BillDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Bill... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends DataLoadingAsyncTask<Bill, Void, Void> {
        DeleteTask(BillDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Bill... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
