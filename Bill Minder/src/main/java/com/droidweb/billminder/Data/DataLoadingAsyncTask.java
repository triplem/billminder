package com.droidweb.billminder.Data;

import android.os.AsyncTask;
import android.support.v4.content.Loader;

/**
 * Created by Droidweb on 8/9/2014.
 */
public abstract class DataLoadingAsyncTask<T1, T2, T3> extends AsyncTask<T1, T2, T3> {
    private Loader<?> loader = null;
    DataLoadingAsyncTask(Loader loader){
        this.loader = loader;
    }

    protected void onPostExecute(){
        loader.onContentChanged();
    }
}
