package com.droidweb.billminder.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.droidweb.billminder.Beans.Bill;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

/**
 * Created by Droidweb on 5/17/2014.
 */
public class BillsDataSource extends AbstractDataSource<Bill>{
    private BillsStoreDatabaseHelper helper;
    public static final String[] allColumns = {BillsStoreDatabaseHelper.COLUMN_ID, BillsStoreDatabaseHelper.COLUMN_PAY_TO, BillsStoreDatabaseHelper.COLUMN_DUE_DATE, BillsStoreDatabaseHelper.COLUMN_ARCHIVED, BillsStoreDatabaseHelper.COLUMN_RECURRENCE_RULE};

    public BillsDataSource(SQLiteDatabase database){
        super(database);
    }

    /*
        public void open(){
            try{
                database = helper.getWritableDatabase();
            }catch (SQLiteException e){
                Log.e(BillsDataSource.this.getClass().getCanonicalName(), e.toString());
            }
        }
    */

    /*
        public void close(){
            helper.close();
        }

    public void createBill(String payTo, Date dueDate, String recurrenceRule){
        if(database==null){
            open();
        }
        Long longDate = dateToLong(dueDate);
        
        ContentValues values = new ContentValues();
        values.put(BillsStoreDatabaseHelper.COLUMN_PAY_TO, payTo);
        values.put(BillsStoreDatabaseHelper.COLUMN_DUE_DATE, longDate);
        values.put(BillsStoreDatabaseHelper.COLUMN_ARCHIVED, false);
        if(recurrenceRule != null){
            values.put(BillsStoreDatabaseHelper.COLUMN_RECURRENCE_RULE, recurrenceRule);
        }
        database.insert(BillsStoreDatabaseHelper.TABLE_BILLS, null, values);
        close();
    }

    public void deleteBill(Bill bill) throws SQLiteException{
        database.delete(BillsStoreDatabaseHelper.TABLE_BILLS, BillsStoreDatabaseHelper.COLUMN_ID + "=?", new String[]{Long.toString(bill.getId())});
    }

    public List<Bill> getAllBills(){
        if(database==null){
            open();
        }
        List<Bill> bills = new ArrayList<Bill>();
        Cursor cursor = database.query(BillsStoreDatabaseHelper.TABLE_BILLS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Bill bill = cursorToBill(cursor);
            bills.add(bill);
            cursor.moveToNext();
        }
        cursor.close();
        return bills;

    }




    */
    @Override
    public boolean insert(Bill entity) {

        ContentValues values = new ContentValues();
        values.put(BillsStoreDatabaseHelper.COLUMN_PAY_TO, entity.getPayTo());
        values.put(BillsStoreDatabaseHelper.COLUMN_DUE_DATE, dateToLong(entity.getDateDue()));
        values.put(BillsStoreDatabaseHelper.COLUMN_ARCHIVED, false);
        if(entity.getRecurrenceRule() != ""){
            values.put(BillsStoreDatabaseHelper.COLUMN_RECURRENCE_RULE, entity.getRecurrenceRule());
        }
        long result = mDatabase.insert(BillsStoreDatabaseHelper.TABLE_BILLS, null, values);
        return result != -1;
    }

    @Override
    public boolean delete(Bill entity) {
        if(entity == null){
            return false;
        }
        int result = mDatabase.delete(BillsStoreDatabaseHelper.TABLE_BILLS,
                BillsStoreDatabaseHelper.COLUMN_ID + "=?", new String[]{Long.toString(entity.getId())});
        return result != 0;
    }

    @Override
    public boolean update(Bill entity) {
        //TODO finish this
        return false;
    }

    @Override
    public List read() {
        List<Bill> bills = new ArrayList<Bill>();
        Cursor cursor = mDatabase.query(BillsStoreDatabaseHelper.TABLE_BILLS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Bill bill = cursorToBill(cursor);
            bills.add(bill);
            cursor.moveToNext();
        }
        cursor.close();
        return bills;
    }

    private Bill cursorToBill(Cursor cursor) {
        Bill bill;
        Log.d("recurrence rule column", Integer.toString(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_RECURRENCE_RULE)));
        if(!cursor.isNull(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_RECURRENCE_RULE))){
            bill = new Bill(cursor.getLong(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_PAY_TO)),
                    cursor.getLong(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_DUE_DATE)),
                    cursor.getInt(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_ARCHIVED)),
                    cursor.getString(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_RECURRENCE_RULE)));
        }else{
            bill = new Bill(cursor.getLong(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_PAY_TO)),
                    cursor.getLong(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_DUE_DATE)),
                    cursor.getInt(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_ARCHIVED)));
        }
        return bill;
    }

    private long dateToLong(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return c.getTimeInMillis();
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return null;
    }
}
