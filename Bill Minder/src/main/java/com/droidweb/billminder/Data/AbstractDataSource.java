package com.droidweb.billminder.Data;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import javax.sql.DataSource;

/**
 * Created by Droidweb on 8/10/2014.
 */
public abstract class AbstractDataSource<T> {
    protected SQLiteDatabase mDatabase;
    public AbstractDataSource(SQLiteDatabase database){
        mDatabase = database;
    }

    public abstract boolean insert(T entity);
    public abstract boolean delete(T entity);
    public abstract boolean update(T entity);
    public abstract List read();
    public abstract List read(String selection, String[] selectionArgs,
                              String groupBy, String having, String orderBy);
}
