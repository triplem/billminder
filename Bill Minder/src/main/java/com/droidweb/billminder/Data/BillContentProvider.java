package com.droidweb.billminder.Data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Droidweb on 8/17/2014.
 */
public class BillContentProvider extends ContentProvider{

    //db
    private BillsStoreDatabaseHelper databaseHelper;
    private static final String AUTHORITY = "com.droidweb.billminder.data.contentprovider";

    private static final int BILLS = 10;
    private static final int BILL_ID = 20;

    private static final String BASE_PATH = "bills";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/bills";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/bill";

    private static final UriMatcher mURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        mURIMatcher.addURI(AUTHORITY, BASE_PATH, BILLS);
        mURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", BILL_ID);
    }

    @Override
    public boolean onCreate() {
        databaseHelper = new BillsStoreDatabaseHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // Set the table
        queryBuilder.setTables(BillsStoreDatabaseHelper.TABLE_BILLS);

        int uriType = mURIMatcher.match(uri);
        switch (uriType) {
            case BILLS:
                break;
            case BILL_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(BillsStoreDatabaseHelper.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = mURIMatcher.match(uri);
        SQLiteDatabase sqlDB = databaseHelper.getWritableDatabase();
        int rowsDeleted = 0;
        long id = 0;
        switch (uriType) {
            case BILLS:
                id = sqlDB.insert(databaseHelper.TABLE_BILLS, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = mURIMatcher.match(uri);
        SQLiteDatabase sqlDB = databaseHelper.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case BILLS:
                rowsDeleted = sqlDB.delete(BillsStoreDatabaseHelper.TABLE_BILLS, selection,
                        selectionArgs);
                break;
            case BILL_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(BillsStoreDatabaseHelper.TABLE_BILLS,
                            BillsStoreDatabaseHelper.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(BillsStoreDatabaseHelper.TABLE_BILLS,
                            BillsStoreDatabaseHelper.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = mURIMatcher.match(uri);
        SQLiteDatabase sqlDB = databaseHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case BILLS:
                rowsUpdated = sqlDB.update(BillsStoreDatabaseHelper.TABLE_BILLS,
                        values,
                        selection,
                        selectionArgs);
                break;
            case BILL_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(BillsStoreDatabaseHelper.TABLE_BILLS,
                            values,
                            BillsStoreDatabaseHelper.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(BillsStoreDatabaseHelper.TABLE_BILLS,
                            values,
                            BillsStoreDatabaseHelper.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = BillsDataSource.allColumns;
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }
}
