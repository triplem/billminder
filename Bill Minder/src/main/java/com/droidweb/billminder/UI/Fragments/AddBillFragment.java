package com.droidweb.billminder.UI.Fragments;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.droidweb.billminder.Beans.Bill;
import com.droidweb.billminder.Data.BillDataLoader;
import com.droidweb.billminder.Data.BillsDataSource;
import com.droidweb.billminder.Data.BillsStoreDatabaseHelper;
import com.droidweb.billminder.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import be.billington.calendar.recurrencepicker.EventRecurrence;
import be.billington.calendar.recurrencepicker.EventRecurrenceFormatter;
import be.billington.calendar.recurrencepicker.RecurrencePickerDialog;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddBillFragment extends Fragment {

    DatePicker dPicker;
    Button addBillButton;
    EditText payToField;
    Spinner repetitionSpinner;
    String recurrenceRule;
    private SQLiteDatabase mDatabase;
    private BillsDataSource mDataSource;
    private BillsStoreDatabaseHelper mDbHelper;

    public AddBillFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDbHelper = new BillsStoreDatabaseHelper(getActivity());
        mDatabase = mDbHelper.getWritableDatabase();
        mDataSource = new BillsDataSource(mDatabase);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_bill, container, false);
        dPicker = (DatePicker) rootView.findViewById(R.id.datePickerFrame);
        addBillButton = (Button) rootView.findViewById(R.id.addBillButton);
        addBillButton.setEnabled(false);
        payToField = (EditText) rootView.findViewById(R.id.payToEditText);
        payToField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                validate();
            }
        });

        repetitionSpinner = (Spinner) rootView.findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        final CustomArrayAdapter adapter = new CustomArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.repetitionOptions)));
        // Apply the adapter to the spinner
        repetitionSpinner.setAdapter(adapter);
        repetitionSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                    RecurrencePickerDialog recurrencePickerDialog = new RecurrencePickerDialog();

                    if (recurrenceRule != null && recurrenceRule.length() > 0) {
                        Bundle bundle = new Bundle();
                        bundle.putString(RecurrencePickerDialog.BUNDLE_RRULE, recurrenceRule);
                        recurrencePickerDialog.setArguments(bundle);
                    }

                    recurrencePickerDialog.setOnRecurrenceSetListener(new RecurrencePickerDialog.OnRecurrenceSetListener() {
                        @Override
                        public void onRecurrenceSet(String rrule) {
                            recurrenceRule = rrule;

                            if (recurrenceRule != null && recurrenceRule.length() > 0) {
                                EventRecurrence recurrenceEvent = new EventRecurrence();
                                recurrenceEvent.setStartDate(new Time("" + dPicker.getCalendarView().getDate()));
                                recurrenceEvent.parse(rrule);
                                String srt = EventRecurrenceFormatter.getRepeatString(getActivity(), getResources(), recurrenceEvent, true);
                                adapter.setText(srt);
                            } else {
                                adapter.setText(getResources().getString(R.string.no_repetition));
                            }
                        }
                    });
                    recurrencePickerDialog.show(getFragmentManager(), "recurrencePicker");
                }
                return true;
            }
        });

        addBillButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // add the bill then dismiss if successful
                Bill bill = new Bill(payToField.getText().toString(), new Date(dPicker.getCalendarView().getDate()), recurrenceRule);
                BillDataLoader mBillDataLoader = new BillDataLoader(getActivity(), mDataSource);
                mBillDataLoader.insert(bill);
                getActivity().finish();
            }
        });
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDbHelper.close();
        mDatabase.close();
        mDataSource = null;
        mDbHelper = null;
        mDatabase = null;
    }

    private void validate() {
        // if the textview is not null, we're good to go
        if (!payToField.getText().toString().equals("")) {
            addBillButton.setEnabled(true);
        } else {
            addBillButton.setEnabled(false);
        }
    }

    private class CustomArrayAdapter extends ArrayAdapter {
        ArrayList<String> values;
        Activity context;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                convertView = inflater.inflate(android.R.layout.simple_spinner_item, null);
            }
            TextView tView1 = (TextView) convertView.findViewById(android.R.id.text1);
            tView1.setText(values.get(position));
            return convertView;
        }

        public CustomArrayAdapter(Activity context, int resource, List<String> objects) {
            super(context, resource, objects);
            this.context = context;
            values = new ArrayList<String>(objects);
        }

        public void setText(String text) {
            values = new ArrayList<String>();
            values.add(text);
            notifyDataSetChanged();
        }
    }
}
