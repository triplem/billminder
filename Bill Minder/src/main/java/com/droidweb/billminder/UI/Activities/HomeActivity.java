package com.droidweb.billminder.UI.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import com.droidweb.billminder.R;
import com.droidweb.billminder.UI.Fragments.BillListingFragment;


public class HomeActivity extends ActionBarActivity{

    public static final int REQUEST_CODE = 0x1337;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create spinner adapter
        SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.billFilter, android.R.layout.simple_spinner_dropdown_item);

        // make the navigation listener we care about
        ActionBar.OnNavigationListener mOnNavigationListener = new ActionBar.OnNavigationListener(){

            String[] navItems = getResources().getStringArray(R.array.billFilter);

            @Override
            public boolean onNavigationItemSelected(int position, long itemID) {
                BillListingFragment billFragment = new BillListingFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                ft.replace(R.id.fragmentContainer, billFragment, navItems[position]);
                ft.commit();
                return true;
            }
        };
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(mSpinnerAdapter, mOnNavigationListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.add_bill) {
            Intent i = new Intent(HomeActivity.this, AddBillActivity.class);
            startActivityForResult(i, REQUEST_CODE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            BillListingFragment billListingFragment = (BillListingFragment)
                    getSupportFragmentManager().findFragmentById(R.id.bill_list_fragment);
            billListingFragment.reload();
        }
    }
}
