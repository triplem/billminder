package com.droidweb.billminder.UI.Activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.droidweb.billminder.R;
import com.droidweb.billminder.UI.Fragments.AddBillFragment;

public class AddBillActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bill);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new AddBillFragment())
                    .commit();
        }
    }
}
