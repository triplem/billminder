package com.droidweb.billminder.UI.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.droidweb.billminder.Beans.Bill;
import com.droidweb.billminder.Data.BillContentProvider;
import com.droidweb.billminder.Data.BillDataLoader;
import com.droidweb.billminder.Data.BillsDataSource;
import com.droidweb.billminder.Data.BillsStoreDatabaseHelper;
import com.droidweb.billminder.R;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eternalmatt on 5/27/14.
 */
public class BillListingFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private List<Bill> mBills = new ArrayList<Bill>();
    private billListAdapter mAdapter;
    private static final int LOADER_ID = 1;

    /**
     * The fragment argument representing the fragment type (archive or outstanding)
     */
    private static final String ARG_FRAGMENT_TYPE = "fragment_type";

    /**
     * Returns a new instance of this fragment for  the given section
     * number.
     */
    public static BillListingFragment newInstance(String type) {
        // TODO: Make the fragment type an enum
        BillListingFragment fragment = new BillListingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FRAGMENT_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public BillListingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bill_view_layout, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new billListAdapter(getActivity(), null, 0);
        setListAdapter(mAdapter);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = { BillsStoreDatabaseHelper.COLUMN_ID, BillsStoreDatabaseHelper.COLUMN_PAY_TO, BillsStoreDatabaseHelper.COLUMN_DUE_DATE };
        CursorLoader cursorLoader = new CursorLoader(getActivity(),
                BillContentProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void reload(){
        getLoaderManager().restartLoader(LOADER_ID, null, this);
    }

    private class billListAdapter extends CursorAdapter {
        private LayoutInflater mInflater;

        public billListAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /*public billListAdapter(Context context, int resourceID, List<Bill> bills){
            super();
        }


        @Override
        public int getCount() {
            return mBills.size();
        }
        @Override
        public Bill getItem(int position) {
            return mBills.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mBills.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.bill_row_layout, parent, false);
            }
            TextView payToField = (TextView) convertView.findViewById(R.id.nameField);
            TextView dueDateField = (TextView) convertView.findViewById(R.id.overdueField);

            payToField.setText(getItem(position).getPayTo());

            // calculate days until due
            Bill bill = getItem(position);

            // TODO: Add how many days until bill in overdue field + add color
            JodaTimeAndroid.init(getActivity());
            DateTime dueDateDt = new DateTime(bill.getDateDue());
            DateTime currentDt = new DateTime();
            int daysDifference = Days.daysBetween(currentDt.toLocalDate(), dueDateDt.toLocalDate()).getDays();

            // depending on what that differential looks like set text / color
            if (daysDifference > 1) {
                dueDateField.setText(Integer.toString(daysDifference) + " Days");
            } else {
                if (daysDifference == 0) {
                    dueDateField.setText("DUE TODAY");
                } else {
                    if (daysDifference < 0) {

                    }
                }
            }


            return convertView;
        }*/

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return mInflater.inflate(R.layout.bill_row_layout, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView payToField = (TextView) view.findViewById(R.id.nameField);
            TextView dueDateField = (TextView) view.findViewById(R.id.overdueField);

            payToField.setText(cursor.getString(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_PAY_TO)));

            JodaTimeAndroid.init(getActivity());
            DateTime dueDateDt = new DateTime(cursor.getColumnIndex(BillsStoreDatabaseHelper.COLUMN_DUE_DATE));
            DateTime currentDt = new DateTime();
            int daysDifference = Days.daysBetween(currentDt.toLocalDate(), dueDateDt.toLocalDate()).getDays();

            // depending on what that differential looks like set text / color
            if (daysDifference > 1) {
                dueDateField.setText(Integer.toString(daysDifference) + " Days");
            } else {
                if (daysDifference == 0) {
                    dueDateField.setText("DUE TODAY");
                } else {
                    if (daysDifference < 0) {

                    }
                }
            }
        }
    }
}
