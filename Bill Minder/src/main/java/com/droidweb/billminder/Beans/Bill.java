package com.droidweb.billminder.Beans;

import java.util.Date;

/**
 * Created by Droidweb on 5/17/2014.
 */
public class Bill {
    public static final String DATE_FORMAT_STRING = "yyyy-MM-dd";
    String payTo;
    Date dateDue;
    long id;
    boolean archived;
    String recurrenceRule = "";

    public Bill(String payTo, Date date, String recurrenceRule){
        this.payTo = payTo;
        this.dateDue = date;
        this.recurrenceRule = recurrenceRule;
        archived = false;
    }

    public Bill(long id, String payTo, Long date, int archived) {
        this.payTo = payTo;
        this.setId(id);
        this.dateDue = new Date(date);
        this.archived = (archived != 0);
    }

    public Bill(long id, String payTo, Long date, int archived, String recurrenceRule) {
        this(id, payTo, date, archived);
        this.recurrenceRule = recurrenceRule;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public Date getDateDue() {
        return dateDue;
    }

    public void setDateDue(Date dateDue) {
        this.dateDue = dateDue;
    }

    public String getRecurrenceRule() {
        return recurrenceRule;
    }

    public void setRecurrenceRule(String recurrenceRule) {
        this.recurrenceRule = recurrenceRule;
    }
}
